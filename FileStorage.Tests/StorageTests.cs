using FileStorage.Tests.Mocks;

namespace FileStorage.Tests;

[TestFixture]
public class StorageTests
{
    [SetUp]
    public void Setup()
    {
        string pathDirOutput = GetOutput(string.Empty);
        // Clear the Output folder before each test.
        if (!Directory.Exists(pathDirOutput))
        {
            Directory.CreateDirectory(pathDirOutput);
        }
        else
        {
            Directory.Delete(pathDirOutput, true);
            Directory.CreateDirectory(pathDirOutput);
        }
    }

    /// <summary>
    /// Checks that the file is copied and that the generated file name does not match the input file name.
    /// </summary>
    /// <param name="fileName">The input file name.</param>
    [TestCase("test.png")]
    public void CopiedAndCorrectGeneratedFileNameTest(string fileName)
    {
        Configuration configuration = new Configuration(GetOutput(string.Empty));
        var factory = new Factory(configuration);

        var storage = factory.CreateStorage();
        string filePathInStorage = storage.SaveFile(GetTestFile(fileName));

        Assert.True(File.Exists(filePathInStorage));

        var outputFileName = Path.GetFileName(filePathInStorage);
        Assert.AreNotEqual(fileName, outputFileName);
    }

    /// <summary>
    /// Tests that everything will be fine if the user wants to expand the record Configuration.
    /// </summary>
    /// <param name="fileName">The input file name.</param>
    [TestCase("test.png")]
    public void CustomConfigurationTest(string fileName)
    {
        Configuration configuration = new CustomConfiguration("CustomConfigurationTest/");

        var factory = new Factory(configuration);

        var storage = factory.CreateStorage();
        string filePathInStorage = storage.SaveFile(GetTestFile(fileName));

        Assert.True(File.Exists(filePathInStorage));
    }

    [TestCase("null")]
    [TestCase("")]
    [TestCase("/")]
    [TestCase("//")]
    [TestCase("///")]
    [TestCase(@"C:/Tempo/sfr")]
    [TestCase(".")]
    public void CreatePath_CorrectInvalidPath_TrowsException(string path)
    {
        Configuration configuration = new Configuration(path);
        var factory = new Factory(configuration);

        var storage = factory.CreateStorage();

        var ex = Assert.Throws<DirectoryNotFoundException>(() => storage.SaveFile(string.Empty));
        Assert.That(ex.Message, Is.EqualTo($"Directory {path} does not exist"));
    }

    [TestCase(@"C:/Tempo/sfr/test1.png")]
    public void InvalidFilePathSpec(string filePath)
    {
        Configuration configuration = new Configuration(GetOutput(string.Empty));
        var factory = new Factory(configuration);

        var storage = factory.CreateStorage();
        var ex = Assert.Throws<FileNotFoundException>(() => storage.SaveFile(filePath));
        Assert.That(ex.Message, Is.EqualTo($"Could not find file path {filePath}!"));
    }

    [Test]
    public void ValidDir()
    {
        Configuration configuration = new Configuration(GetOutput(string.Empty).TrimEnd(new[] { '/' }));
        var factory = new Factory(configuration);

        var storage = factory.CreateStorage();
        string filePathInStorage = storage.SaveFile(GetTestFile("test.png"));

        Assert.True(File.Exists(filePathInStorage));
    }

    /// <summary>
    /// Gets the absolute path to the output test file.
    /// </summary>
    /// <param name="fileName">File name of the output file.</param>
    /// <returns>The absolute path to the output test file.</returns>
    private static string GetOutput(string fileName)
    {
        return Path.Combine(Environment.CurrentDirectory, @"../../../Output/", fileName);
    }

    /// <summary>
    /// Gets the absolute path to the test file.
    /// </summary>
    /// <param name="fileName">File name of the test file.</param>
    /// <returns>The absolute path to the test file.</returns>
    private static string GetTestFile(string fileName)
    {
        return Path.Combine(Environment.CurrentDirectory, "TestData", fileName);
    }
}