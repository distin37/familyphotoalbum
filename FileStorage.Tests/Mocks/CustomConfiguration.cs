﻿namespace FileStorage.Tests.Mocks
{
    /// <summary>
    /// It is possible that the user will want to expand the record Configuration to suit his needs.
    /// This class shows how the user could act in this case.
    /// </summary>
    public record CustomConfiguration  : Configuration
    {
        private static readonly string RootDir = Path.Combine(Environment.CurrentDirectory, @"../../../Output/");

        public CustomConfiguration(string fileStorageDir) : base(fileStorageDir)
        {
            this.FileStorageDir = Path.Combine(RootDir, fileStorageDir);
            Directory.CreateDirectory(FileStorageDir);
        }
    }
}