﻿namespace FileStorage
{
    /// <summary>
    /// Initialization class for working with the FileStorage Class Library
    /// </summary>
    public class Factory
    {
        private readonly Configuration _configuration;

        /// <summary>
        /// Passes the configuration value to an instance of the Class(s).
        /// </summary>
        /// <param name="configuration">configuration value</param>
        public Factory(Configuration configuration)
        {
            if (!configuration.FileStorageDir.EndsWith("/"))
            {
                _configuration = configuration with { FileStorageDir = $"{configuration.FileStorageDir}/" };
            }
            _configuration = configuration;
        }

        /// <summary>
        /// Creates an instance of the <see cref="Storage"/>.
        /// </summary>
        /// <returns>Returns(Creates) An instance of the <see cref="Storage"/> with the specified path with the directory file.</returns>
        public Storage CreateStorage()
        {
            return new Storage(_configuration);
        }
    }
}