﻿namespace FileStorage
{
    /// <summary>
    /// Sets an entry to the path of the media content directory.
    /// </summary>
    public record Configuration(string FileStorageDir);

}