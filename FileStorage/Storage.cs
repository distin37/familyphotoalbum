﻿using Storage.Contract;

namespace FileStorage
{

    /// <summary>
    /// The class works with the media content storage directory.
    /// </summary>
    public class Storage : IFileStorage

    {
    private readonly Configuration _configuration;

    internal Storage(Configuration configuration)
    {
        _configuration = configuration;
    }

    /// <summary>
    ///  Saves the file by copying it to the media content directory, returns a link to the media content object.
    /// </summary>
    /// <param name="pathFile">the file path on the client</param>
    /// <returns>The path of the file on the server.</returns>
    /// <exception cref="DirectoryNotFoundException">Invalid path specified.</exception>
    /// <exception cref="FileNotFoundException">The file was not found.</exception>
    public string SaveFile(string pathFile)
    {
        ValidateDir();
        ValidPathFile(pathFile);

        var newFilePath = GetFilePath(pathFile);

        File.Copy(pathFile, newFilePath, false);

        return newFilePath;
    }

    private string GenerateNewFilePath(string pathFile)
    {
        var ext = Path.GetExtension(pathFile);
        var newFileName = $"{Guid.NewGuid().ToString()}{ext}";

        return Path.Combine(_configuration.FileStorageDir, newFileName);
    }

    private string GetFilePath(string pathFile)
    {
        string newFilePath;

        do
        {
            newFilePath = GenerateNewFilePath(pathFile);
        } while (File.Exists(newFilePath));

        return newFilePath;
    }

    private void ValidateDir()
    {
        string path = _configuration.FileStorageDir.Trim(new[] { ' ', '/', '.' });
        if (!Directory.Exists(_configuration.FileStorageDir) || path.Length == 0)
        {
            throw new DirectoryNotFoundException($"Directory {_configuration.FileStorageDir} does not exist");
        }
    }

    private void ValidPathFile(string pathFile)
    {
        if (!File.Exists(pathFile))
        {
            throw new FileNotFoundException($"Could not find file path {pathFile}!");
        }
    }
    }

}