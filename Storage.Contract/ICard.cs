﻿namespace Storage.Contract
{

    public interface ICard
    {
        string MediaFilePath { get; }
        string Description { get; }
        bool IsEmpty { get; }
    }

}