﻿
namespace Storage.Contract
{

    public interface ICardStorage
    {
        int AddCard(ICard cardData);

        ICard GetCard(int id);

        ICollection<ICard> GetAllCards();
    }

}