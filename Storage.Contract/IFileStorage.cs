﻿

namespace Storage.Contract;

public interface IFileStorage
{
    string SaveFile(string pathFile);
}