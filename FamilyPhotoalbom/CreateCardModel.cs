﻿using Storage.Contract;

namespace FamilyPhotoalbom
{

    /// <summary>
    ///     Album card
    /// </summary>
    public class CreateCardModel : ICard
    {
        /// <summary>
        ///     the line contains a link to a photo or video
        /// </summary>
        public required string MediaFilePath { get; init; }

        /// <summary>
        ///     The string contains a description of the photo or video.
        /// </summary>
        public string Description { get; init; }

        public bool IsEmpty => MediaFilePath == String.Empty && Description == String.Empty;
    }

}