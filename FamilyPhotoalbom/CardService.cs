﻿using Storage.Contract;

namespace FamilyPhotoalbom
{

    public class CardService
    {
        private ICardStorage CardStorage { get; }

        public CardService(ICardStorage cardStorage)
        {
            CardStorage = cardStorage;
        }

        public int AddCard(ICard card)
        {
            return CardStorage.AddCard(card);
        }

        public ICard GetCard(int id)
        {
            return CardStorage.GetCard(id);
        }

        public ICollection<ICard> GetAllCards()
        {
            return CardStorage.GetAllCards();
        }
    }

}