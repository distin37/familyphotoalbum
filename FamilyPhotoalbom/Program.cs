﻿namespace FamilyPhotoalbom
{

    internal class Programm
    {
        private static void Main(string[] args)
        {
            var storage = new Storage();
            var cardService = new CardService(storage);
            var uiActions = new UIActions(cardService);
            uiActions.OpenMenuAction();
        }
    }

}