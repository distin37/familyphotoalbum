﻿using Storage.Contract;

namespace FamilyPhotoalbom
{

    public class Storage : ICardStorage
    {
        private static readonly Card EmptyCard = new() { MediaFilePath = "", Description = "", Id = Card.InvalidId };
        private readonly Dictionary<int, Card> _cards = new();

        public int AddCard(ICard cardData)
        {
            var card = new Card
            {
                Id = GenerateId(), MediaFilePath = cardData.MediaFilePath, Description = cardData.Description
            };

            _cards.Add(card.Id, card);

            return card.Id;
        }

        public ICard GetCard(int id)
        {
            return _cards.GetValueOrDefault(id, EmptyCard);
        }

        public ICollection<ICard> GetAllCards()
        {
            var cards = new List<ICard>(_cards.Values);
            return cards;
        }

        private int GenerateId()
        {
            return _cards.Keys.Count == 0 ? 1 : _cards.Keys.Max() + 1;
        }
    }

}