﻿using Storage.Contract;

namespace FamilyPhotoalbom
{
    public static class GuardUserExtensions
    {
        private static readonly string[] SupportedExtensions = { ".jpg", ".png", ".mp4" };

        public static bool IsValidExtensions(this ICard card)
        {
            var ext = Path.GetExtension(card.MediaFilePath);

            foreach (var supExt in SupportedExtensions)
            {
                if (ext == supExt)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsValidMediaPath(this ICard card)
        {
            if (File.Exists(card.MediaFilePath))
            {
                return true;
            }

            return false;
        }
    }
}