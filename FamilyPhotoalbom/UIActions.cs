﻿using System.Diagnostics;
using Storage.Contract;

namespace FamilyPhotoalbom
{

    internal class UIActions
    {
        private readonly CardService _cardService;

        public UIActions(CardService cardService)
        {
            _cardService = cardService;
        }

        public void CreateCard()
        {
            Console.Write(" Прикрепите Медиа:");
            var mediaPath = Console.ReadLine();
            Console.Write("Добавьте описание:");
            var desc = Console.ReadLine();
            var createCard = new CreateCardModel { MediaFilePath = mediaPath, Description = desc };
            var id = _cardService.AddCard(createCard);
            Console.Write($"Карточка {id} создана.");
            OpenMenuAction();
        }

        public void GetCard()
        {
            Console.Write(" Введите ID Карточки:");
            var id = Convert.ToInt32(Console.ReadLine());
            var card = _cardService.GetCard(id);
            if (card.IsEmpty)
            {
                Console.Write(" Карточка не найдена");
            }
            else
            {
                ShowContent(card);
            }

            OpenMenuAction();
        }

        public void GetAllCards()
        {
            var cards = _cardService.GetAllCards();
            foreach (var card in cards)
            {
                Console.WriteLine($" Медиа: {card.MediaFilePath}\n Описание: {card.Description}\n");
            }

            OpenMenuAction();
        }

        private void ShowContent(ICard card)
        {
            if (!card.IsValidExtensions())
            {
                Console.WriteLine("Неверное расширение");
            }

            if (!card.IsValidMediaPath())
            {
                Console.WriteLine("Нет медиафайла");
            }
            else
            {
                Console.Write($"Описание: {card.Description}");
                Process.Start("explorer.exe", card.MediaFilePath);
            }
        }

        public void OpenMenuAction()
        {
            Console.Write(
                " Выберите действие\n 1.Добавить карточку\n 2.Получить карточку\n 3.Получить все карточки\n:");
            var action = Convert.ToInt32(Console.ReadLine());
            switch (action)
            {
                case 1:
                    CreateCard();
                    break;
                case 2:
                    GetCard();
                    break;
                case 3:
                    GetAllCards();
                    break;
            }
        }
    }

}