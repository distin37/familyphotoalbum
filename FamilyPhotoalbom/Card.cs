﻿using Storage.Contract;

namespace FamilyPhotoalbom
{

    public class Card : ICard
    {
        public const int InvalidId = -1;
        public int Id { get; init; }
        public string MediaFilePath { get; init; }
        public string Description { get; init; }

        public bool IsEmpty => Id == InvalidId;
    }

}